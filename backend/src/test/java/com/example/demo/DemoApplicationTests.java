package com.example.demo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class DemoApplicationTest {

    @Autowired
    private MockMvc mockMvc;
    
    /**
     * #1 Überprüft, ob ein Task-Objekt mit einer bestimmten Taskbeschreibung erstellt werden kann.
     */
    @Test
    void contextLoads() {
        Task t = new Task();

        t.setTaskdescription("Test123");
        assertEquals("Test123", t.getTaskdescription());
        // fail("Not yet implemented");
    }

    /**
     * #2 Überprüft, ob eine neue Aufgabe über eine HTTP-POST-Anfrage hinzugefügt werden kann.
     * 
     * @throws Exception
     */
    @Test
    void addTaskTest() throws Exception {
        mockMvc.perform(post("/tasks")
                .content("{\"taskdescription\":\"Test Task\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    /**
     * #3 Überprüft, ob eine Aufgabe über eine HTTP-POST-Anfrage gelöscht werden kann.
     * 
     * @throws Exception
     */
    @Test
    void delTaskTest() throws Exception {
        mockMvc.perform(post("/delete")
                .content("{\"taskdescription\":\"Test Task\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    /**
     * #4 Überprüft, ob die Liste aller Aufgaben über eine HTTP-GET-Anfrage abgerufen werden kann.
     * 
     * @throws Exception
     */
    @Test
    void getTasksTest() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk());
    }

    /**
     * #5 TDD:
     * Testet das Hinzufügen eines Tasks mit ungültigem Text: 
     * überprüft, ob eine Fehlermeldung ausgegeben wird, wenn der Task-Text leer ist oder eine ungültige Länge hat.
     * 
     * @throws Exception
     */
    @Test
    void addTaskWithDueDateTest() throws Exception {
        mockMvc.perform(post("/tasks")
                .content("{\"taskdescription\":\"\",\"dueDate\":\"2023-02-14\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
        
        mockMvc.perform(post("/tasks")
                .content("{\"taskdescription\":\"abc\",\"dueDate\":\"2023-02-14\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
        
        mockMvc.perform(post("/tasks")
                .content("{\"dueDate\":\"2023-02-14\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
	/**
 * #6 TDD:
 * Überprüft das Hinzufügen eines Tasks mit einem ungültigen Fertigstellungsdatum: 
 * überprüft, ob eine Fehlermeldung ausgegeben wird, wenn das Fertigstellungsdatum in der Vergangenheit liegt oder ungültig ist.
 * 
 * @throws Exception
 */
@Test
void addTaskWithDueDateAndCompletedTest() throws Exception {
    mockMvc.perform(post("/tasks")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\"taskdescription\":\"Test Task\",\"dueDate\":\"2023-02-14\",\"completed\":true}"))
        .andExpect(status().isOk());
}

/**
 * #7 TDD:
 * Überprüft das Bearbeiten eines Tasks: 
 * überprüft, ob ein Task bearbeitet werden kann, indem man seinen Text oder das Fertigstellungsdatum aktualisiert.
 * 
 * @throws Exception
 */
@Test
void getTaskByDescriptionTest() throws Exception {
    mockMvc.perform(post("/tasks")
            .content("{\"taskdescription\":\"Test Task\"}"))
        .andExpect(status().isOk());
    mockMvc.perform(get("/tasks/Test Task"))
        .andExpect(status().isOk());
}
/**
 * #8 TDD:
 * Überprüft das Markieren eines Tasks als abgeschlossen: 
 * überprüft, ob ein Task als abgeschlossen markiert werden kann, indem man einen entsprechenden Indikator im UI hinzufügt.
 * 
 * @throws Exception
 */
@Test
void markTaskAsCompletedTest() throws Exception {
    // add a new task
    mockMvc.perform(post("/tasks")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\"taskdescription\":\"Test Task\"}"))
        .andExpect(status().isOk());

    // mark the task as completed
    mockMvc.perform(post("/tasks/completed")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\"taskdescription\":\"Test Task\",\"completed\":true}"))
        .andExpect(status().isOk());

    // get the task and check if it is completed
    mockMvc.perform(get("/tasks/Test Task"))
        .andExpect(jsonPath("$.completed").value(true));
}
}



	


 

