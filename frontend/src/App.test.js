import { render, screen } from '@testing-library/react';
import App from './App';


// Testet, ob die Überschrift der Anwendung gerendert wird.
test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

// Testet das Hinzufügen einer Aufgabe.
test('allows user to add a new task', async () => {
  // Die App wird gerendert.
  render(<App />);
  
  // Die Eingabe- und Schaltflächenelemente werden abgerufen.
  const inputElement = screen.getByLabelText(/Enter new Task/i);
  const addButtonElement = screen.getByRole('button', { name: /Save/i });
  
  // Ein neuer Aufgabenname wird erstellt.
  const taskName = 'Buy_groceries';
  
  // Die Eingabe wird aktualisiert und die Schaltfläche wird geklickt.
  fireEvent.change(inputElement, { target: { value: taskName } });
  fireEvent.click(addButtonElement);
  
  // Es wird gewartet, bis das neue Element gerendert wurde und dann wird geprüft, ob es angezeigt wird.
  await waitFor(() => {
    const newTaskElement = screen.getByText(/Buy_groceries/);
    expect(newTaskElement).toBeInTheDocument();
  });
});

//Überprüft, ob die Komponente nach dem Rendern die korrekten Startzustände aufweist.
 
test('renders with initial state', () => {
  // Die App wird gerendert.
  render(<App />);
  
  // Die Eingabe- und Schaltflächenelemente werden abgerufen und geprüft.
  const inputElement = screen.getByLabelText(/Enter new Task/i);
  const addButtonElement = screen.getByRole('button', { name: /Save/i });
  expect(inputElement).toHaveValue('');
  expect(addButtonElement).toBeInTheDocument();
  
  // Das Element mit dem Text 'Task 1:' sollte nicht gerendert werden.
  expect(screen.queryByText(/Task 1:/)).not.toBeInTheDocument();
});

// Überprüft, ob das Hinzufügen mehrerer Aufgaben nacheinander korrekt funktioniert.
 
test('allows user to add multiple tasks', async () => {
  // Die App wird gerendert und die Eingabe- und Schaltflächenelemente werden abgerufen.
  render(<App />);
  const inputElement = screen.getByLabelText(/Enter new Task/i);
  const addButtonElement = screen.getByRole('button', { name: /Save/i });

  // Eine neue Aufgabe wird hinzugefügt.
  const task1Name = 'Buy_groceries';
  fireEvent.change(inputElement, { target: { value: task1Name } });
  fireEvent.click(addButtonElement);

  // Eine weitere neue Aufgabe wird hinzugefügt.
  const task2Name = 'Do_laundry';
  fireEvent.change(inputElement, { target: { value: task2Name } });
  fireEvent.click(addButtonElement);

  // Die App wird erneut gerendert und es wird gewartet, bis die neuen Elemente gerendert wurden und dann wird geprüft, ob sie angezeigt werden.
  render(<App />);
  await waitFor(() => {
    const newTaskElement = screen.getByText(/Task 1: Buy_groceries/);
    expect(newTaskElement).toBeInTheDocument();
  });
  const newTaskElement2 = screen.getByText(/Task 2: Do_laundry/);
  expect(newTaskElement2).toBeInTheDocument();
});



